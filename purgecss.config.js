module.exports = {
  content: ["./src/**/*.elm", "index.js", "index.html"],
  extractors: [
    {
      extractor: (content) => {
        return content.match(/[A-Za-z0-9-_&:@<>\/]+/g) || [];
      },
      extensions: ["elm", "html", "js"],
    },
  ],
};
