module Main exposing (main)

-- DEPENDENCIES

import Browser
import Browser.Dom exposing (Viewport, getViewport)
import Browser.Events exposing (onAnimationFrameDelta, onResize)
import Html exposing (Html, div, span, text)
import Html.Attributes exposing (class, style)
import Html.Events exposing (onClick)
import Task
import Vector exposing (Vector, add, scale)



-- CONSTANTS


initialPosition : Vector
initialPosition =
    Vector 20 20


initialVelocity : Vector
initialVelocity =
    Vector 800 700


initialRotation : Float
initialRotation =
    0


size : Size
size =
    { w = 40
    , h = 40
    }



-- MODEL


type alias Size =
    { w : Int
    , h : Int
    }


type alias Model =
    { shouldMove : Bool
    , velocity : Vector
    , rotation : Float
    , position : Vector
    , windowSize : Size
    }


windowSizeFromViewport : Viewport -> Msg
windowSizeFromViewport { viewport } =
    GotNewWindowSize (round viewport.width) (round viewport.height)


getWindowSizeCmd : Cmd Msg
getWindowSizeCmd =
    Task.perform windowSizeFromViewport getViewport


init : () -> ( Model, Cmd Msg )
init _ =
    ( Model False initialVelocity initialRotation initialPosition (Size 0 0)
    , getWindowSizeCmd
    )



-- UPDATE


type Msg
    = Tick Float
    | GotNewWindowSize Int Int
    | MakeCatMove


move : Vector -> Vector -> Float -> Vector
move position velocity deltaInS =
    add position (scale (deltaInS * -0.2) velocity)


rotate : Vector -> Float
rotate position =
    toFloat (modBy size.w (round (position.x * 0.5))) / toFloat size.w


calculateCollisionVelocity : Size -> Vector -> Vector -> Vector
calculateCollisionVelocity windowSize position velocity =
    let
        vx =
            if position.x + toFloat size.w >= toFloat windowSize.w || position.x <= 0 then
                velocity.x * -0.7

            else
                velocity.x

        vy =
            if position.y + toFloat size.h > toFloat windowSize.h || position.y <= 0 then
                velocity.y * -0.7

            else
                velocity.y
    in
    Vector vx vy


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        MakeCatMove ->
            ( { model | shouldMove = not model.shouldMove }, Cmd.none )

        GotNewWindowSize w h ->
            ( { model | windowSize = Size w h }, Cmd.none )

        Tick deltaInMs ->
            let
                { shouldMove, velocity, position, windowSize, rotation } =
                    model

                deltaInS =
                    deltaInMs / 1000

                nextPosition =
                    if shouldMove then
                        move position velocity deltaInS

                    else
                        position

                nextVelocity =
                    calculateCollisionVelocity windowSize nextPosition velocity

                nextRotation =
                    if shouldMove then
                        rotate position

                    else
                        rotation
            in
            ( { model
                | position = nextPosition
                , velocity = nextVelocity
                , rotation = nextRotation
              }
            , Cmd.none
            )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.batch
        [ onAnimationFrameDelta Tick
        , onResize GotNewWindowSize
        ]



-- VIEW


cat : Bool -> Html Msg
cat shouldMove =
    let
        face =
            if shouldMove then
                "🙀"

            else
                "😼"
    in
    span [ class "cat", onClick MakeCatMove ] [ text face ]


view : Model -> Html Msg
view model =
    let
        { position, rotation, shouldMove } =
            model

        left =
            String.fromFloat position.x ++ "px"

        top =
            String.fromFloat position.y ++ "px"

        transform =
            "rotate(" ++ String.fromFloat rotation ++ "turn)"
    in
    div
        [ class "position:absolute cursor:pointer"
        , style "top" top
        , style "left" left
        , style "transform" transform
        ]
        [ cat shouldMove ]



-- MAIN


main : Program () Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }
