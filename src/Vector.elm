module Vector exposing (Vector, add, scale)


type alias Vector =
    { x : Float
    , y : Float
    }


add : Vector -> Vector -> Vector
add a b =
    Vector (a.x + b.x) (a.y + b.y)


scale : Float -> Vector -> Vector
scale scalar { x, y } =
    Vector (x * scalar) (y * scalar)
